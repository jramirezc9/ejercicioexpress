
const env = process.env;

const config = {
  db: { /* do not put password or any sensitive info here, done only for demo */
    host: env.DB_HOST || '192.168.1.27',
    port: env.DB_PORT || '5432',
    user: env.DB_USER || 'postgres',
    password: env.DB_PASSWORD || '123',
    database: env.DB_NAME || 'ejercicio1',
  },
  listPerPage: env.LIST_PER_PAGE || 10,
};

module.exports = config;
